package dev.rehm.data;

import dev.rehm.models.Vehicle;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

//implements vehicleDao
public class VehicleData {

    private List<Vehicle> items = new ArrayList<>();

    public VehicleData(){
        super();

        items.add(new Vehicle(1,"Nissan", "Sentra", 2018, 20000,24));
        items.add(new Vehicle(2,"Toyota", "Corolla", 2019, 25000,94));
        items.add(new Vehicle(3,"Honda", "Civic", 2020, 30000,10));
    }

    public List<Vehicle> getAllVehicles(){
        return new ArrayList<>(items);
    }

    public Vehicle getVehicleById(int id){
        for(Vehicle v: items){
            if(v.getId()==id)
                return v;
        }

        return null;
    }

    public Vehicle addNewVehicle(Vehicle v){
        items.add(v);
        return v;
    }


    public void deleteVehicle(int id){

//        items.removeIf(marketItem -> (marketItem!=null)?id==marketItem.getId():false);
            Predicate<Vehicle> idCheck = vehicle -> vehicle!=null && id==vehicle.getId();
            items.removeIf(idCheck);
            // this remove operation can also be handled using iteration, looping through and using the
            // list remove method

    }

    public Vehicle updateVehicle(Vehicle newVehicle){

        Vehicle idCheck = items.stream().filter(item -> item.getId() == newVehicle.getId()). findAny().orElse(null);
        int index = items.indexOf(idCheck);
        System.out.println("indexof "+items.indexOf(idCheck)+" new item id "+newVehicle.getId() + " idchek: "+ newVehicle);

        items.set(index, newVehicle);
        return idCheck;


    }

}
