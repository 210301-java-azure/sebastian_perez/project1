package dev.rehm.data;

import dev.rehm.models.User;

public interface UserDao {

    public User authenticateUser(String username, String password);

    public User getUserByUsername(String username);
}
