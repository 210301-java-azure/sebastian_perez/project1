package dev.rehm.data;

import dev.rehm.models.PreOwnedVehicle;


import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class PreOwnedVehicleData {

    private List<PreOwnedVehicle> items = new ArrayList<>();

    public PreOwnedVehicleData(){
        super();

        items.add(new PreOwnedVehicle(4,"BMW", "3Series", 2016, 30000,24,1,15000,
                formatDate(2019,10,06)));
        items.add(new PreOwnedVehicle(5,"Mazda", "Miata", 2012, 16000,94,2, 9000,
                formatDate(2020,10,06)));
        items.add(new PreOwnedVehicle(6,"Nissan", "Juke", 2016, 22000,10,3,15000,
                formatDate(2021,02,06)));
    }

    public List<PreOwnedVehicle> getAllItems(){
        return new ArrayList<>(items);
    }


    public Date formatDate(int year, int month, int day){

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month - 1); // <-- months start at 0.
        cal.set(Calendar.DAY_OF_MONTH, day);

        java.sql.Date date = new java.sql.Date(cal.getTimeInMillis());

        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd-yyyy");
        System.out.println(sdf.format(date));

        return date;

    }
}
