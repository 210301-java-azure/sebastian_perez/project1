package dev.rehm.data;

import dev.rehm.models.Vehicle;

import java.util.List;

public interface VehicleDao {

    public List<Vehicle> getAllVehicles();
    public Vehicle getVehicleById(int id);
    public Vehicle addNewVehicle(Vehicle vehicle);
    public void deleteVehicle(int id);
    public Vehicle updateVehicle(Vehicle newVehicle);
    public List<Vehicle> getVehiclesInPriceRange(int min, int max);
    public List<Vehicle> getVehiclesWithMaxPrice(int max);
    public List<Vehicle> getVehiclesWithMinPrice(int min);


}
