package dev.rehm.data;

import dev.rehm.models.Vehicle;
import dev.rehm.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class VehicleDaoImpl implements VehicleDao{

    private Logger logger = LoggerFactory.getLogger(VehicleDaoImpl.class);

    @Override
    public List<Vehicle> getAllVehicles() {
        List<Vehicle> vehicles = new ArrayList<>();

        // try with resources - Connection is able to be used here because it implements Autocloseable
        try (Connection connection = ConnectionUtil.getConnection()){
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("select * from vehicles");

            // result set pointer is before the first record

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set
                int id = resultSet.getInt("id");
                String make = resultSet.getString("make");
                String model = resultSet.getString("model");
                int year = resultSet.getInt("year");
                double price = resultSet.getDouble("price");
                int mileage = resultSet.getInt("mileage");
                Vehicle vehicle = new Vehicle(id, make, model, year, price, mileage);
                vehicles.add(vehicle);
            }
            logger.info("selecting all vehicles from db - "+ vehicles.size()+ " vehicles retrieved");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return vehicles;
    }

    @Override
    public Vehicle getVehicleById(int id) {
        try(Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("select * from vehicles where id = ?");
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                String make = resultSet.getString("make");
                String model = resultSet.getString("model");
                int year = resultSet.getInt("year");
                double price = resultSet.getDouble("price");
                int mileage = resultSet.getInt("mileage");
                logger.info("1 item retrieved from database by id: " + id);
                return new Vehicle(id, make, model, year, price, mileage);
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return null;
    }

    @Override
    public Vehicle addNewVehicle(Vehicle vehicle) {
        try(Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement("insert into vehicles (make, model, year, price, mileage) values (?, ?, ?, ?, ?)");
            preparedStatement.setString(1, vehicle.getMake());
            preparedStatement.setString(2, vehicle.getModel());
            preparedStatement.setInt(3, vehicle.getYear());
            preparedStatement.setDouble(4, vehicle.getPrice());
            preparedStatement.setInt(5, vehicle.getMileage());
            preparedStatement.executeUpdate();
            logger.info("successfully added new item to the db");
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return vehicle;
    }

    @Override
    public void deleteVehicle(int id) {
        try (Connection connection = ConnectionUtil.getConnection()) {
            PreparedStatement preparedStatement = connection.prepareStatement("delete from vehicles where id = ?");
            preparedStatement.setInt(1,id);
            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                String make = resultSet.getString("make");
                String model = resultSet.getString("model");
                int year = resultSet.getInt("year");
                double price = resultSet.getDouble("price");
                int mileage = resultSet.getInt("mileage");
                logger.info("1 item retrieved from database by id: " + id);
                Vehicle v= new Vehicle(id, make, model, year, price, mileage);
                System.out.println(v.toString());
            }

        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }

    }

    @Override
    public Vehicle updateVehicle(Vehicle newVehicle) {
        try(Connection connection = ConnectionUtil.getConnection()){

            PreparedStatement preparedStatement = connection.prepareStatement(
                    "UPDATE vehicles SET (make, model, year, price, mileage) = (?, ?, ?, ?, ?)\n" +
                            "  WHERE id = ?\n" +
                            "    RETURNING id, make, model, year, price, mileage;");

            preparedStatement.setString(1, newVehicle.getMake());
            preparedStatement.setString(2, newVehicle.getModel());
            preparedStatement.setInt(3, newVehicle.getYear());
            preparedStatement.setDouble(4, newVehicle.getPrice());
            preparedStatement.setInt(5, newVehicle.getMileage());
            preparedStatement.setInt(6, newVehicle.getId());

            ResultSet resultSet = preparedStatement.executeQuery();
            if(resultSet.next()){
                int id = resultSet.getInt("id");
                String make = resultSet.getString("make");
                String model = resultSet.getString("model");
                int year = resultSet.getInt("year");
                double price = resultSet.getDouble("price");
                int mileage = resultSet.getInt("mileage");
                return new Vehicle(id, make, model, year, price, mileage);

            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
        return newVehicle;
    }

    @Override
    public List<Vehicle> getVehiclesInPriceRange(int min, int max) {
        List<Vehicle> vehiclesList = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement("select * from vehicles where price < ? and price > ?");
            preparedStatement.setInt(1,max);
            preparedStatement.setInt(2,min);

            ResultSet resultSet = preparedStatement.executeQuery();


            // result set pointer is before the first record

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set
                int id = resultSet.getInt("id");
                String make = resultSet.getString("make");
                String model = resultSet.getString("model");
                int year = resultSet.getInt("year");
                double price = resultSet.getDouble("price");
                int mileage = resultSet.getInt("mileage");
                Vehicle v = new Vehicle(id, make, model, year, price, mileage);
                vehiclesList.add(v);
            }
            logger.info("selecting all items from db - "+ vehiclesList.size()+ " items retrieved");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return vehiclesList;
    }

    @Override
    public List<Vehicle> getVehiclesWithMaxPrice(int max) {
        List<Vehicle> vehiclesList = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement("select * from vehicles where price < ?");
            preparedStatement.setInt(1,max);

            ResultSet resultSet = preparedStatement.executeQuery();


            // result set pointer is before the first record

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set
                int id = resultSet.getInt("id");
                String make = resultSet.getString("make");
                String model = resultSet.getString("model");
                int year = resultSet.getInt("year");
                double price = resultSet.getDouble("price");
                int mileage = resultSet.getInt("mileage");
                Vehicle v = new Vehicle(id, make, model, year, price, mileage);
                vehiclesList.add(v);
            }
            logger.info("selecting all items from db - "+ vehiclesList.size()+ " items retrieved");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return vehiclesList;
    }

    @Override
    public List<Vehicle> getVehiclesWithMinPrice(int min) {
        List<Vehicle> vehiclesList = new ArrayList<>();
        try (Connection connection = ConnectionUtil.getConnection()){
            PreparedStatement preparedStatement = connection.prepareStatement("select * from vehicles where price > ?");
            preparedStatement.setInt(1,min);

            ResultSet resultSet = preparedStatement.executeQuery();


            // result set pointer is before the first record

            while(resultSet.next()){ // moves the pointer to the next record, returning false if there are no more record
                //process each record in the result set
                int id = resultSet.getInt("id");
                String make = resultSet.getString("make");
                String model = resultSet.getString("model");
                int year = resultSet.getInt("year");
                double price = resultSet.getDouble("price");
                int mileage = resultSet.getInt("mileage");
                Vehicle v = new Vehicle(id, make, model, year, price, mileage);
                vehiclesList.add(v);
            }
            logger.info("selecting all items from db - "+ vehiclesList.size()+ " items retrieved");
        } catch (SQLException e) {
            logger.error(e.getClass()+ " " +e.getMessage());
        }
        return vehiclesList;
    }

}
