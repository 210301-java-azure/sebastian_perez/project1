package dev.rehm.data;

import dev.rehm.models.User;
import dev.rehm.util.ConnectionUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class UserDaoImpl implements UserDao {

    private Logger logger = LoggerFactory.getLogger(UserDaoImpl.class);

    @Override
    public User authenticateUser(String username, String password) {

        try(Connection connection = ConnectionUtil.getConnection()){

            PreparedStatement preparedStatement = connection.prepareStatement("select username, password, role from " +
                    "user_credentials" +
                    " " +
                    "where  username = ? and password = ? ");
            preparedStatement.setString(1, username);
            preparedStatement.setString(2, password);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {

                String usernameDB = resultSet.getString(UserMapping.USERNAME);
                String roleDB = resultSet.getString(UserMapping.ROLE);
                logger.info("UserDaoImpl ", usernameDB, " role: ", roleDB);
                User user= new User(usernameDB , roleDB);
                return user;
            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
//        return "Invalid user credentials";
        return null;
    }

    @Override
    public User getUserByUsername(String username) {
        try(Connection connection = ConnectionUtil.getConnection()){

            PreparedStatement preparedStatement = connection.prepareStatement("select username from " +
                    "user_credentials" +
                    " " +
                    "where  username = ? ");
            preparedStatement.setString(1, username);
            ResultSet resultSet = preparedStatement.executeQuery();

            if(resultSet.next()) {

                String usernameDB = resultSet.getString(UserMapping.USERNAME);

                User user= new User(usernameDB);
                return user;

            }
        } catch (SQLException e) {
            logger.error(e.getClass() + " " + e.getMessage());
        }
//        return "Invalid user credentials";
        return null;
    }



    private static class UserMapping {
        private static final String ID = "id";
        private static final String USERNAME = "username";
        private static final String ROLE = "role";
    }

}
