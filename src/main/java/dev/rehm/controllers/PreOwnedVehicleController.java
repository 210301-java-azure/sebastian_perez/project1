package dev.rehm.controllers;

import dev.rehm.data.PreOwnedVehicleData;
import dev.rehm.models.PreOwnedVehicle;

import io.javalin.http.Context;

public class PreOwnedVehicleController {


    public PreOwnedVehicleController() {

    }

    public PreOwnedVehicle addVehicle(PreOwnedVehicle v) {
        return v;
    }

    public void addAllPreOwnedVehicle(Context ctx) {

            PreOwnedVehicleData data = new PreOwnedVehicleData();

            ctx.json(data.getAllItems());
        }

}
