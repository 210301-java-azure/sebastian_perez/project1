package dev.rehm.controllers;


import dev.rehm.models.Vehicle;
import dev.rehm.services.VehicleService;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class VehicleController {
    Logger logger = LoggerFactory.getLogger(VehicleController.class);

    private VehicleService service= new VehicleService();

    //    private VehicleData data = new VehicleData();
//    public VehicleController() { }

    public void handleGetAllVehiclesRequest(Context ctx){
        //ctx.result(data.getAllItems().toString());}
        logger.info("getting all vehicles");
        ctx.json(service.getAll()); // json method converts object to JSON
    }

    public void handleGetVehicleByIdRequest(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")) {

            int idInput = Integer.parseInt(idString);
            Vehicle v = service.getById(idInput);
            if (v == null) {
                logger.warn("No vehicle found with id:" + idInput);
                throw new NotFoundResponse("No vehicle found with provided id: " + idInput);
            } else {
                logger.info("getting vehicle with id: " + idInput);
                ctx.json(v);
            }
        }else{
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }

    }


    public void handlePostNewVehicle(Context ctx){
        Vehicle v = ctx.bodyAsClass(Vehicle.class);
        logger.info("adding new item: "+ v);
        service.add(v);
        ctx.status(201);
    }

    public void handleDeleteVehicleById(Context ctx){
        String idString = ctx.pathParam("id");
        if(idString.matches("^\\d+$")){
            int idInput = Integer.parseInt(idString);
            logger.info("deleting record with id: "+idInput);
            service.delete(idInput);
        } else {
            throw new BadRequestResponse("input \""+idString+"\" cannot be parsed to an int");
        }

    }

    public void handleUpdateVehicleById(Context ctx){
        Vehicle v = ctx.bodyAsClass(Vehicle.class);
//        logger.info("updating  item: "+ v);
        service.update(v);
        ctx.status(201);
    }

    public void handleGetVehiclesRequest(Context ctx){
        String maxPrice = ctx.queryParam("max-price");
        String minPrice = ctx.queryParam("min-price");
        if(maxPrice!=null || minPrice!=null){
            logger.info("getting items in price range");
            ctx.json(service.getPriceRange(minPrice, maxPrice));
        } else { // /items with no query params
            //ctx.result(data.getAllItems().toString());}
            logger.info("getting all items");
            ctx.json(service.getAll()); // json method converts object to JSON
        }
    }
}

//        app.get("/items/:id", ctx->{
//            VehicleData data = new VehicleData();
//            String stringId= ctx.pathParam("id");
//        });

