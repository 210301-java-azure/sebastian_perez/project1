package dev.rehm.controllers;

import dev.rehm.models.AuthenticationResponse;
import dev.rehm.models.User;
import dev.rehm.services.UserService;
import dev.rehm.util.JwtUtil;
import io.javalin.http.Context;
import io.javalin.http.UnauthorizedResponse;
import org.eclipse.jetty.client.HttpResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.Charset;
import java.util.Random;

public class AuthController {

    private Logger logger = LoggerFactory.getLogger(AuthController.class);
    private UserService service = new UserService();
    private JwtUtil jwtTokenUtil = new JwtUtil();
    private AuthenticationResponse authenticationResponse;
    String token ;
    public void authenticateLogin(Context ctx){
        // getting params from what would be a form submission (Content-Type: application/x-www-form-urlencoded)
        String user = ctx.formParam("username");
        String pass = ctx.formParam("password");

        User userDetails = service.authenticate(user , pass);


        if(userDetails!=null) {
            logger.info("successful login");

            token = jwtTokenUtil.generateToken(userDetails);
            authenticationResponse = new AuthenticationResponse(token);
            ctx.header("Authorization", token);
            ctx.status(200);
//            return ResponseEntity.ok(new AuthenticationResponse(token));
            return;
        }
        logger.info("Credentials were incorrect");
        throw new UnauthorizedResponse("Credentials were incorrect");
    }

    public void authorizeToken(Context ctx){
        logger.info("attempting to authorize token");

        // getting the "Authorization" header from the incoming request
        String authHeader = ctx.header("Authorization");

        if(authHeader!=null){
            String username = jwtTokenUtil.extractUsername(authHeader);
            User userDetails = this.service.loadUserByUsername(username);

            if(jwtTokenUtil.validateToken(authHeader, userDetails)) {
                logger.info("validate token successful");
                //Assign role rules with access manager
            }
        } else {
            logger.warn("improper authorization");
            throw new UnauthorizedResponse("token was incorrect");
        }
    }


}
