package dev.rehm;

import dev.rehm.controllers.AuthController;
import dev.rehm.controllers.PreOwnedVehicleController;
import dev.rehm.controllers.VehicleController;
import io.javalin.Javalin;

import static io.javalin.apibuilder.ApiBuilder.*;

public class JavalinApp {


    AuthController authController = new AuthController();
    VehicleController vehicleController = new VehicleController();

//    PreOwnedVehicleController povc = new PreOwnedVehicleController();

    Javalin app = Javalin.create().routes(()->{
        path("/items", ()->{
            before("/", authController::authorizeToken);
            get(vehicleController::handleGetVehiclesRequest);
            post(vehicleController::handlePostNewVehicle);
            put(vehicleController::handleUpdateVehicleById);
            path("/:id",()->{
                before("/", authController::authorizeToken);
                get(vehicleController::handleGetVehicleByIdRequest);
                delete(vehicleController::handleDeleteVehicleById);
            });
        });
        path("/login",()->{
            post("/", authController::authenticateLogin);
        });
    });


    public void start(int port) {
        this.app.start(port);
    }

    public void stop() {
        this.app.stop();
    }

}

//        app.get("/items", ctx->{      vehicleController.handleGetVehiclesRequest(ctx);  });
//        app.get("/items", vehicleController::handleGetVehiclesRequest); // json method converts object to JSON
//
//        // the second parameter is a method reference (CRUD)
//        app.get("/items/:id",vehicleController::handleGetVehicleByIdRequest);
//        app.post("/items", vehicleController::handlePostNewVehicle);
//        app.delete("/items/:id", vehicleController::handleDeleteVehicleById);
//        app.put("/items", vehicleController:: handleUpdateVehicleById);

