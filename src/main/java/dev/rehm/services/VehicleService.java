package dev.rehm.services;

import dev.rehm.data.VehicleDao;
import dev.rehm.data.VehicleDaoImpl;
import dev.rehm.models.Vehicle;

import java.util.List;

public class VehicleService {

    private VehicleDao vehicleDao = new VehicleDaoImpl();

    public List<Vehicle> getAll(){
        return vehicleDao.getAllVehicles();
    }

    public Vehicle getById(int id){
        return vehicleDao.getVehicleById(id);
    }

    public Vehicle add(Vehicle v){
        return vehicleDao.addNewVehicle(v);
    }

    public void delete(int id){
        vehicleDao.deleteVehicle(id);
    }

    public Vehicle update(Vehicle v){
        return vehicleDao.updateVehicle(v);
    }

    public List<Vehicle> getPriceRange(String minPrice, String maxPrice) {

        if (maxPrice != null) {
            // max provided
            // parse string input to doubles
            int max = Integer.parseInt(maxPrice);

            if (minPrice != null) {
                //if both are provided
                // parse string input to doubles
                int min = Integer.parseInt(minPrice);
                return vehicleDao.getVehiclesInPriceRange(min, max);
            }
            // only max provided
            return vehicleDao.getVehiclesWithMaxPrice(max);
        } else {
            // only min provided
            // parse string input to doubles
            int min = Integer.parseInt(minPrice);
            return vehicleDao.getVehiclesWithMinPrice(min);
        }
    }
}

