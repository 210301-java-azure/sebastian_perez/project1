package dev.rehm.services;

import dev.rehm.controllers.AuthController;
import dev.rehm.data.UserDao;
import dev.rehm.data.UserDaoImpl;
import dev.rehm.models.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserService {
    private Logger logger = LoggerFactory.getLogger(UserService.class);

    private UserDao userDao = new UserDaoImpl();
    public User authenticate(String username , String password){
        return userDao.authenticateUser(username, password);
    }

    public User loadUserByUsername(String username) {
        return userDao.getUserByUsername(username);
    }

}
