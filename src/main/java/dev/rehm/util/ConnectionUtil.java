package dev.rehm.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtil {


    private static Connection connection;

    private ConnectionUtil(){
        super();
    }

    public static synchronized Connection getConnection() throws SQLException {

        if (connection == null || connection.isClosed()) {
            try {
                Class.forName("org.postgresql.Driver");
            } catch (ClassNotFoundException e) {
                System.out.println("Could not register driver!");
                e.printStackTrace();
            }
            System.out.println("Opening new connection...");

            String CONNECTION_USERNAME = System.getenv("usernameDB");
            String CONNECTION_PASSWORD = System.getenv("password");
            String URL = System.getenv("connectionUrl");

            connection = DriverManager.getConnection(URL, CONNECTION_USERNAME, CONNECTION_PASSWORD);

        }

        return connection;
    }

}
