package dev.rehm.models;

import java.io.Serializable;
import java.sql.Date;

public class PreOwnedVehicle extends Vehicle implements Serializable {

    private int preOwnedId;
    private double purchasedPrice;


    private Date mechanicInspection;

    public PreOwnedVehicle(){

    }

    public PreOwnedVehicle(int preOwnedId, double purchasedPrice, Date mechanicInspection) {
        this.preOwnedId = preOwnedId;
        this.purchasedPrice = purchasedPrice;
        this.mechanicInspection = mechanicInspection;
    }

    public PreOwnedVehicle(
            int id,
            String make,
            String model,
            int year,
            double price,
            int mileage,
            int preOwnedId,
            double purchasedPrice,
            Date mechanicInspection
    ) {
        super(id, make, model, year, price, mileage);
        this.preOwnedId = preOwnedId;
        this.purchasedPrice = purchasedPrice;
        this.mechanicInspection = mechanicInspection;
    }

    public int getPreOwnedId() {
        return preOwnedId;
    }

    public void setPreOwnedId(int preOwnedId) {
        this.preOwnedId = preOwnedId;
    }

    public double getPurchasedPrice() {
        return purchasedPrice;
    }

    public void setPurchasedPrice(double purchasedPrice) {
        this.purchasedPrice = purchasedPrice;
    }

    public Date getMechanicInspection() {
        return mechanicInspection;
    }

    public void setMechanicInspection(Date mechanicInspection) {
        this.mechanicInspection = mechanicInspection;
    }

    @Override
    public String toString() {
        return "PreOwnedVehicle{" +
                "preOwnedId=" + preOwnedId +
                ", purchasedPrice=" + purchasedPrice +
                ", mechanicInspection=" + mechanicInspection +
                '}';
    }




}
