package dev.rehm.controllers;

import dev.rehm.JavalinApp;
import dev.rehm.models.Vehicle;
import kong.unirest.GenericType;
import kong.unirest.HttpResponse;
import kong.unirest.Unirest;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class VehicleControllerIntegrationTest {

    private static JavalinApp app = new JavalinApp();

    @BeforeAll
    public static void startService(){
        app.start(7001);
    }

    @AfterAll
    public static void stopService(){
        app.stop();
    }

    @Test
    public void testGetAllVehiclesUnauthorized(){
        HttpResponse<String> response = Unirest.get("http://localhost:7001/items").asString();
        assertAll(
                ()->assertEquals( 401,response.getStatus()),
                ()->assertEquals( "Unauthorized",response.getBody()));
    }

    @Test
    public void testGetAllVehiclesAuthorized(){
        HttpResponse<List<Vehicle>> response = Unirest.get("http://localhost:7001/items")
                .header("Authorization",
                        "eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJjcmVobURCQWRtaW4iLCJpc0FkbWluIjp0cnVlLCJleHAiOjE2MTYxMzU1NDQsImlhdCI6MTYxNjA5OTU0NH0.s5GAV8iy1ZxF9pRgwVK_6XCi5GMFKzh0bOKJz_Ykhew")
                .asObject(new GenericType<List<Vehicle>>() {});
        assertAll(
                ()->assertEquals(200,response.getStatus()),
                ()->assertTrue(response.getBody().size()>0)
        );
    }

}
