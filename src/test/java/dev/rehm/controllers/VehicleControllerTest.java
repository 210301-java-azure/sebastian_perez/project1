package dev.rehm.controllers;

import dev.rehm.models.Vehicle;
import dev.rehm.services.VehicleService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import io.javalin.http.Context;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

public class VehicleControllerTest {
    @InjectMocks
    private VehicleController vehicleController;

    /*
    We created a mock service here, a dummy object which we can give behavior by stubbing its methods
    We could also create a spy, which is a real object whose methods can be stubbed and invoked instead of the real methods
     */
    @Mock
    private VehicleService service;

    @BeforeEach
    public void initMocks(){
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAllVehiclesHandler(){
        Context context = mock(Context.class);

        List<Vehicle> vehiclesListMock = new ArrayList<>();
        vehiclesListMock.add(new Vehicle(1,"Nissan", "Sentra", 2018, 20000,24));
        vehiclesListMock.add(new Vehicle(2,"Toyota", "Corolla", 2019, 25000,94));
        vehiclesListMock.add(new Vehicle(3,"Honda", "Civic", 2020, 30000,10));

        when(service.getAll()).thenReturn(vehiclesListMock);
        vehicleController.handleGetVehiclesRequest(context);
        verify(context).json(vehiclesListMock);
    }

    @Test
    public void testGetVehiclesInRangeWithMin(){
        Context context = mock(Context.class);

        List<Vehicle> vehiclesListMock = new ArrayList<>();
        vehiclesListMock.add(new Vehicle(1,"Nissan", "Sentra", 2018, 20000,24));
        vehiclesListMock.add(new Vehicle(2,"Toyota", "Corolla", 2019, 25000,94));

        when(service.getPriceRange("19999", null)).thenReturn(vehiclesListMock);
        when(context.queryParam("min-price")).thenReturn("19999");

        vehicleController.handleGetVehiclesRequest(context);
        verify(context).json(vehiclesListMock);
    }


    @Test
    public void testGetItemsInRangeWithMax(){
        Context context = mock(Context.class);

        List<Vehicle> vehiclesListMock = new ArrayList<>();
        vehiclesListMock.add(new Vehicle(1,"Nissan", "Sentra", 2018, 20000,24));
        vehiclesListMock.add(new Vehicle(2,"Toyota", "Corolla", 2019, 25000,94));

        when(service.getPriceRange(null, "30000")).thenReturn(vehiclesListMock);
        when(context.queryParam("max-price")).thenReturn("30000");

        vehicleController.handleGetVehiclesRequest(context);
        verify(context).json(vehiclesListMock);
    }

    @Test
    public void testGetItemsInRange(){
        Context context = mock(Context.class);

        List<Vehicle> vehiclesListMock = new ArrayList<>();
        vehiclesListMock.add(new Vehicle(1,"Nissan", "Sentra", 2018, 20000,24));

        when(service.getPriceRange("19000", "25000")).thenReturn(vehiclesListMock);
        when(context.queryParam("min-price")).thenReturn("19000");
        when(context.queryParam("max-price")).thenReturn("25000");

        vehicleController.handleGetVehiclesRequest(context);
        verify(context).json(vehiclesListMock);
    }




}
